terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.67.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
    null = {
      source  = "hashicorp/null"
      version = "3.1.0"
    }
  }
  required_version = ">= 0.15"
}

provider "google" {
  project = "gitlab-production"
  region  = "us-east1"
}

resource "google_bigquery_dataset" "dataset" {
  dataset_id                 = "vnagy_json_extract_table"
  friendly_name              = "vnagy_json_extract_table"
  description                = "Terraform analysis for vnagy"
  location                   = "US"
  delete_contents_on_destroy = true
}

# Pre table contains one line of JSON per row
resource "google_bigquery_table" "preload_table" {
  dataset_id = google_bigquery_dataset.dataset.dataset_id
  table_id   = "vnagy_terraform_XXXanalysis_pre"

  # 6 hour expiration
  expiration_time = 21600000

  schema = <<-EOF
    [{
      "name": "json",
      "type": "STRING",
      "mode": "NULLABLE",
      "description": "JSON"
    }]
  EOF
}

#terraform import random_integer.job_load_sequence 2,1,1000
resource "random_integer" "job_load_sequence" {
  min  = 1
  max  = 1000
  seed = "attempt5"
}

# Pre load job treats JSON GCS files as CSV, reading each line into the preload table
# This is followed by a second job which parses the JSON into a normal table structure
resource "google_bigquery_job" "preload_job" {
  job_id = "pre_load_${random_integer.job_load_sequence.result}"

  load {
    ignore_unknown_values = true
    field_delimiter       = "±"
    max_bad_records       = 100
    source_format         = "CSV"
    autodetect            = false

    source_uris = [
      "gs://gitlab-gprd-logging-archive/rails.api/2021/05/18/*",
    ]

    destination_table {
      project_id = google_bigquery_table.preload_table.project
      dataset_id = google_bigquery_table.preload_table.dataset_id
      table_id   = google_bigquery_table.preload_table.table_id
    }

    skip_leading_rows     = 0
    schema_update_options = []

    create_disposition = "CREATE_NEVER"
    write_disposition  = "WRITE_APPEND"
  }
}

resource "google_bigquery_table" "json_extract_table" {
  deletion_protection = false
  dataset_id          = google_bigquery_dataset.dataset.dataset_id
  table_id            = "terraform_analysis"
}

# terraform will return when the job is created, but will not wait for the job to complete
# Ensure that the preload job has completed before running the main load job
resource "null_resource" "wait_for_preload" {
  provisioner "local-exec" {
    command = "bq --project_id ${google_bigquery_job.preload_job.project} wait ${google_bigquery_job.preload_job.job_id}"
  }
  triggers = {
    job = google_bigquery_job.preload_job.job_id
  }
  depends_on = [google_bigquery_job.preload_job]
}

resource "google_bigquery_job" "json_extract_job" {
  job_id     = "load_job_${random_integer.job_load_sequence.result}"
  depends_on = [google_bigquery_job.preload_job, null_resource.wait_for_preload]

  query {
    destination_table {
      table_id = google_bigquery_table.json_extract_table.id
    }
    write_disposition = "WRITE_TRUNCATE"

    query = <<-EOF
      SELECT
        PARSE_TIMESTAMP("%FT%H:%M:%E*SZ", JSON_EXTRACT_SCALAR(json, "$.timestamp")) as timestamp,
        JSON_EXTRACT_SCALAR(json, "$.jsonPayload['path']") as path,
        JSON_EXTRACT_SCALAR(json, "$.jsonPayload['ua']") as ua,
        JSON_EXTRACT_SCALAR(json, "$.jsonPayload['route']") as route,
        CAST(JSON_EXTRACT_SCALAR(json, "$.jsonPayload['status']") as INT64) as status,
      FROM `${google_bigquery_table.preload_table.project}".${google_bigquery_table.preload_table.dataset_id}.${google_bigquery_table.preload_table.table_id}`;
    EOF
  }
}

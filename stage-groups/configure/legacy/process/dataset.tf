resource "google_bigquery_table" "json_extract_table" {
  deletion_protection = false
  dataset_id          = "${data.terraform_remote_state.pre.dataset_id}"
  table_id            = "${local.table_id}"
}

# terraform will return when the job is created, but will not wait for the job to complete
# Ensure that the preload job has completed before running the main load job
resource "null_resource" "wait_for_preload" {
  provisioner "local-exec" {
    command = "bq --project_id ${data.terraform_remote_state.pre.project_id} wait ${data.terraform_remote_state.pre.preload_job_id}"
  }
}

#terraform import random_integer.job_load_sequence 2,1,1000
resource "random_integer" "job_load_sequence" {
  min  = 1
  max  = 1000
  seed = "attempt5"
}

resource "google_bigquery_job" "json_extract_job" {
  job_id     = "load_job_${random_integer.job_load_sequence.result}"
  depends_on = [null_resource.wait_for_preload]

  query {
    destination_table {
      table_id = google_bigquery_table.json_extract_table.id
    }
    write_disposition = "WRITE_TRUNCATE"

    query = templatefile(extract.sql, {
      from="${data.terraform_remote_state.pre.preload_table_project}"."${data.terraform_remote_state.pre.preload_table_dataset}"."${data.terraform_remote_state.pre.preload_table_table}"
    })
  }
}

## Setup

To set up a new processing, you should overwrite at least

- `extract.sql`: contains the SQL run to generate the final table from the `pre` table
- `locals` in `main.tf`: contain the necessary settings to create the final table

## TODO

- Move into a module

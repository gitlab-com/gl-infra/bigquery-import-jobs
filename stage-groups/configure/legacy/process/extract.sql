SELECT
  JSON_EXTRACT_SCALAR(json, "$.insertId") as insertId,
  PARSE_TIMESTAMP("%FT%H:%M:%E*SZ", JSON_EXTRACT_SCALAR(json, "$.timestamp")) as timestamp,
  JSON_EXTRACT_SCALAR(json, "$.jsonPayload['duration_s']") as duration_s,
  JSON_EXTRACT_SCALAR(json, "$.jsonPayload['method']") as method,
  JSON_EXTRACT_SCALAR(json, "$.jsonPayload['path']") as path,
  JSON_EXTRACT_SCALAR(json, "$.jsonPayload['ua']") as ua,
  $TABLE_NAME.vnagy_user_agent_explode(JSON_EXTRACT_SCALAR(json, "$.jsonPayload['ua']"))[offset(0)] as ua_family,
  $TABLE_NAME.vnagy_user_agent_explode(JSON_EXTRACT_SCALAR(json, "$.jsonPayload['ua']"))[SAFE_OFFSET(1)] as version_like,
  REGEXP_EXTRACT($TABLE_NAME.vnagy_user_agent_explode(JSON_EXTRACT_SCALAR(json, "$.jsonPayload['ua']"))[SAFE_OFFSET(1)], r"^[0-9.]+") as version,
  JSON_EXTRACT_SCALAR(json, "$.jsonPayload['user_id']") as user_id,
  JSON_EXTRACT_SCALAR(json, "$.jsonPayload['route']") as route,
  CAST(JSON_EXTRACT_SCALAR(json, "$.jsonPayload['status']") as INT64) as status,
FROM `${from}`;

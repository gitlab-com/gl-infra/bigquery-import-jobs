data "terraform_remote_state" "pre" {
  backend = "http"
  config = {
    address  = "${var.remote_address_base}/${local.project}--pre"
    username = "${var.gitlab_username}"
    password = "${var.gitlab_password}"
  }
}

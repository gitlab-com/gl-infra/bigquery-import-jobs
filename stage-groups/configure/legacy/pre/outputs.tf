output "dataset_id" {
  value = google_bigquery_dataset.dataset.dataset_id
}
output "project_id" {
  value = google_bigquery_job.preload_job.project
}
output "preload_job_id" {
  value = google_bigquery_job.preload_job.job_id
}
output "preload_table_project" {
  value = google_bigquery_table.preload_table.project
}
output "preload_table_dataset" {
  value = google_bigquery_table.preload_table.dataset_id
}
output "preload_table_table" {
  value = google_bigquery_table.preload_table.table_id
}

terraform {
  backend "http" {}
}

locals {
  dataset_id    = "gitlab-production:vnagy_terraform_analysis"
  friendly_name = "vnagy_terraform_analysis"
  table_id      = "vnagy_terraform_analysis_pre"
  source_uris = [
    "gs://gitlab-gprd-logging-archive/rails.api/2021/05/18/*",
  ]
}

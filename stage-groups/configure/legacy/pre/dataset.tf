resource "google_bigquery_dataset" "dataset" {
  dataset_id                 = local.dataset_id
  friendly_name              = local.friendly_name
  description                = "API analysis for vnagy"
  location                   = "US"
  delete_contents_on_destroy = true
}

# Pre table contains one line of JSON per row
resource "google_bigquery_table" "preload_table" {
  dataset_id = google_bigquery_dataset.dataset.dataset_id
  table_id   = local.table_id

  # 6 hour expiration
  expiration_time = 21600000

  schema = <<-EOF
    [{
      "name": "json",
      "type": "STRING",
      "mode": "NULLABLE",
      "description": "JSON"
    }]
  EOF
}

#terraform import random_integer.job_load_sequence 2,1,1000
resource "random_integer" "job_load_sequence" {
  min  = 1
  max  = 1000
  seed = "attempt5"
}

# Pre load job treats JSON GCS files as CSV, reading each line into the preload table
# This is followed by a second job which parses the JSON into a normal table structure
resource "google_bigquery_job" "preload_job" {
  job_id = "pre_load_${random_integer.job_load_sequence.result}"

  load {
    ignore_unknown_values = true
    field_delimiter       = "±"
    max_bad_records       = 100
    source_format         = "CSV"
    autodetect            = false

    source_uris = local.source_uris

    destination_table {
      project_id = google_bigquery_table.preload_table.project
      dataset_id = google_bigquery_table.preload_table.dataset_id
      table_id   = google_bigquery_table.preload_table.table_id
    }

    skip_leading_rows     = 0
    schema_update_options = []

    create_disposition = "CREATE_NEVER"
    write_disposition  = "WRITE_APPEND"
  }
}

#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

GCP_PROJECT=gitlab-production
TABLE_NAME=vnagy_terraform_analysis

bq --project_id "$GCP_PROJECT" \
  load \
  --source_format=CSV \
  --field_delimiter "±" \
  --max_bad_records 100 \
  --replace \
  --ignore_unknown_values \
  vnagy_terraform_analysis.${TABLE_NAME}_pre \
  gs://gitlab-gprd-logging-archive/rails.api/2021/03/* \
  json:STRING

bq --project_id "$GCP_PROJECT" \
  load \
    --source_format=CSV \
    --field_delimiter "±" \
    --max_bad_records 100 \
    --noreplace \
    --ignore_unknown_values \
     vnagy_terraform_analysis.${TABLE_NAME}_pre \
    gs://gitlab-gprd-logging-archive/rails.api/2021/04/* \
    json:STRING

bq --project_id "$GCP_PROJECT" \
  load \
    --source_format=CSV \
    --field_delimiter "±" \
    --max_bad_records 100 \
    --noreplace \
    --ignore_unknown_values \
     vnagy_terraform_analysis.${TABLE_NAME}_pre \
    gs://gitlab-gprd-logging-archive/rails.api/2021/05/* \
    json:STRING

read -r -d '' query <<EOF || true
  CREATE OR REPLACE TABLE \`gitlab-production.vnagy_terraform_analysis.${TABLE_NAME}\` AS
  SELECT
    PARSE_TIMESTAMP("%FT%H:%M:%E*SZ", JSON_EXTRACT_SCALAR(json, "$.timestamp")) as timestamp,
    JSON_EXTRACT_SCALAR(json, "$.jsonPayload['path']") as path,
    JSON_EXTRACT_SCALAR(json, "$.jsonPayload['ua']") as ua,
    JSON_EXTRACT_SCALAR(json, "$.jsonPayload['route']") as route,
    CAST(JSON_EXTRACT_SCALAR(json, "$.jsonPayload['status']") as INT64) as status,
  FROM \`gitlab-production.vnagy_terraform_analysis.${TABLE_NAME}_pre\`
EOF

bq --project_id "$GCP_PROJECT" \
  query \
  --nouse_legacy_sql "$query"

#!/usr/bin/env bash
## This requires the bq command line tool: https://cloud.google.com/bigquery/docs/bq-command-line-tool​

set -euo pipefail
IFS=$'\n\t'

GCP_PROJECT=gitlab-production
TABLE_NAME=vnagy_terraform_analysis
TABLE_NAME_PRE=vnagy_terraform_analysis_pre


# Import a bucket as CSV as our field names are dotted

# bq --project_id "$GCP_PROJECT" \
#   load \
#   --source_format=CSV \
#   --field_delimiter "±" \
#   --max_bad_records 100 \
#   --replace \
#   --ignore_unknown_values \
#   vnagy_terraform_analysis.${TABLE_NAME}_pre \
#   gs://gitlab-gprd-logging-archive/rails.production/2021/02/27/* \
#   json:STRING
# ​
# bq --project_id "$GCP_PROJECT" \
#   load \
#     --source_format=CSV \
#     --field_delimiter "±" \
#     --max_bad_records 100 \
#     --noreplace \
#     --ignore_unknown_values \
#      vnagy_terraform_analysis.${TABLE_NAME}_pre \
#     gs://gitlab-gprd-logging-archive/rails.production/2019/11/* \
#     json:STRING

# Convert the fooling CSV table to proper JSON fields

read -r -d '' query <<EOF || true
  CREATE OR REPLACE TABLE \`$GCP_PROJECT.$TABLE_NAME.$TABLE_NAME\` AS

  SELECT
    PARSE_TIMESTAMP("%FT%H:%M:%E*SZ", JSON_EXTRACT_SCALAR(json, "$.timestamp")) as timestamp,
    JSON_EXTRACT_SCALAR(json, "$.jsonPayload['path']") as path,
    JSON_EXTRACT_SCALAR(json, "$.jsonPayload['ua']") as ua,
    $TABLE_NAME.vnagy_user_agent_explode(JSON_EXTRACT_SCALAR(json, "$.jsonPayload['ua']"))[offset(0)] as ua_family,
    $TABLE_NAME.vnagy_user_agent_explode(JSON_EXTRACT_SCALAR(json, "$.jsonPayload['ua']"))[SAFE_OFFSET(1)] as version_like,
    REGEXP_EXTRACT($TABLE_NAME.vnagy_user_agent_explode(JSON_EXTRACT_SCALAR(json, "$.jsonPayload['ua']"))[SAFE_OFFSET(1)], r"^[0-9.]+") as version,
    JSON_EXTRACT_SCALAR(json, "$.jsonPayload['user_id']") as user_id,
    JSON_EXTRACT_SCALAR(json, "$.jsonPayload['route']") as route,
    CAST(JSON_EXTRACT_SCALAR(json, "$.jsonPayload['status']") as INT64) as status,
  FROM \`$GCP_PROJECT.$TABLE_NAME.$TABLE_NAME_PRE\`
EOF

bq --project_id "$GCP_PROJECT" \
  query \
  --nouse_legacy_sql "$query"

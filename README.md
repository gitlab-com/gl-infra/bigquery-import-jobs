# BigQuery Import Jobs

This repository contains BigQuery import job definitions.

The idea is that these definitions will be maintained and executed manually, but over time we will add validation, verification, and automation.

The goal being that we allow BigQuery users to self-service manage their data jobs.

## Add a new data project

1. Start a new MR
1. Copy an existing Terraform-based directory (example `terraform`)
1. Follow the readme in its `pre` and `process` directories
1. Update `.gitlab-ci.yml` to run the script on changes for the new directory too by extending the `changes` section
